library("ggplot2")
DA=function(pref_student,pref_teacher,cap_teacher,M,W){
  
  applicable_student=rep(1,length=M) #highest applicable rank
  matched_student=rep(0,length=M) # 0 if not matched, 1 if matched
  matching=matrix(0,nrow=M,ncol=W+1)
  filled_teacher=rep(0,length=W) # number of currently accepted students
  eval_teacher=rep(0,length=W)
  result=list()
  counter=0
  while(any(matched_student==0)){
    counter = counter+1
    # for all unmatched students
    for(st in which(matched_student==0)){
      pref=pref_student[st,]
      apl=applicable_student[st] #rank to apply
      applyto=which(pref==apl)
      if(applyto==(W+1)){
        #(below or)equal to the requirement of student
        matching[st,W+1]=1 # assigned to W+1
        matched_student[st]=1
      }else{
        # apply to somewhere
        if(pref_teacher[applyto,M+1]<pref_teacher[applyto,st]){
          # below the requirement of teacher
          # do nothing
        }else{
          # both sides' requirements satisfied
          eval_teacher[applyto]=eval_teacher[applyto]+1
          if(cap_teacher[applyto]==filled_teacher[applyto]){
            #occupied
            mylevel=pref_teacher[applyto,st]
            others=which(matching[,applyto]==1)
            otherslevel=max(pref_teacher[applyto,others])
            weakest=which(pref_teacher[applyto,]==otherslevel)
            if(otherslevel>mylevel){
              # win
              matching[st,applyto]=1
              matched_student[st]=1
              matching[weakest,applyto]=0
              matched_student[weakest]=0
            }else{
              # lose
              # do nothing
            }
          }else{
            # unoccupied -> accept
            matched_student[st]=1
            matching[st,applyto]=1
            filled_teacher[applyto]= filled_teacher[applyto]+1
          }
        }
        applicable_student[st] = applicable_student[st]+1
      }    
    }
  }
  result$matching=matching
  result$pref_student=pref_student
  result$pref_teacher=pref_teacher
  result$cap_teacher=cap_teacher
  result$iteration=counter
  result$eval_teacher=eval_teacher
  result$filled_teacher=filled_teacher
  result$unmatched=which(matching[,W+1]==1)
  return(result)
}

M=300
W=30

#change cap
cap_range=1:20
mean_eval=c()
num_unmatched=c()
for(CAP in cap_range){
  print(paste("start",CAP))
  students=1:M
  teachers=1:W
  #pref_student[st,te]=rank of te for st
  pref_student=matrix(0,nrow=M,ncol=W+1)
  pref_teacher=matrix(0,nrow=W,ncol=M+1)
  cap_teacher=rep(0,length=W)
  
  for(st in students){
    pref_student[st,]=c(resample(teachers,W,replace=F),W+1)
  }
  for(te in teachers){
    pref_teacher[te,]=c(resample(students,M,replace=F),M+1)
    cap_teacher[te]=CAP
  }
  result=DA(pref_student,pref_teacher,cap_teacher,M,W)
  mean_eval=c(mean_eval,mean(result$eval_teacher))
  num_unmatched=c(num_unmatched,length(result$unmatched))
}

num_unmatched_ratio=num_unmatched/M
title_str=paste("M=",M,",W=",W,sep="")
g1=ggplot(data.frame(cbind(cap_range,mean_eval)),aes(cap_range,mean_eval))+ggtitle(paste(title_str,"来る人数(平均)"))
gg1=g1 + geom_point() + geom_step()+xlab("定員")+ylab("来る人数（平均）")
g2=ggplot(data.frame(cbind(cap_range,num_unmatched_ratio)),aes(cap_range,num_unmatched_ratio))+ggtitle(paste(title_str,"ゼミ難民率"))
gg2=g2 + geom_point() + geom_step()+xlab("定員")+ylab("ゼミ難民率")
g3=ggplot(data.frame(cbind(mean_eval,num_unmatched_ratio)),aes(mean_eval,num_unmatched_ratio))+ggtitle(paste(title_str,"来る人数(平均)VSゼミ難民率"))
gg3=g3 + geom_point() + geom_smooth()+xlab("来る人数（平均）")+ylab("ゼミ難民率")


#heterogenous threshold

M=300
W=30
CAP=10

mean_eval=c()
num_unmatched=c()
students=1:M
teachers=1:W
#pref_student[st,te]=rank of te for st

eval_teacher=c()
filled_teacher=c()
thr_teacher_all=c()
for(i in 1:20){
  pref_student=matrix(0,nrow=M,ncol=W+1)
  pref_teacher=matrix(0,nrow=W,ncol=M+1)
  cap_teacher=rep(0,length=W)

  # set params
  seiseki_student=runif(M) # quantile of seiseki
  thr_teacher=(runif(W)*0.9) # from 0.5 to 1.0
  
  for(st in students){
    pref_student[st,]=c(resample(teachers,W,replace=F),W+1)
  }
  for(te in teachers){
    upper=which(seiseki_student>thr_teacher[te])
    lower=which(seiseki_student<=thr_teacher[te])
    pref_teacher[te,upper]=resample(1:length(upper),length(upper),replace=F)
    pref_teacher[te,lower]=resample((length(upper)+2):(M+1),length(lower),replace=F)
    pref_teacher[te,M+1]=length(upper)+1
    cap_teacher[te]=CAP
  }
  result=DA(pref_student,pref_teacher,cap_teacher,M,W)
  thr_teacher_all=c(thr_teacher_all,thr_teacher)
  eval_teacher=c(eval_teacher,result$eval_teacher)
  filled_teacher=c(filled_teacher,result$filled_teacher)
}

title_str=paste("M=",M,",W=",W,",CAP=",CAP,sep="")
g1=ggplot(data.frame(cbind(thr_teacher_all,eval_teacher)),aes(thr_teacher_all,eval_teacher))+ggtitle(paste(title_str,"評価回数VS足切り率"))
gg4=g1 + geom_point()+stat_smooth()+xlab("足切り率")+ylab("評価回数")
g2=ggplot(data.frame(cbind(thr_teacher_all,filled_teacher)),aes(thr_teacher_all,filled_teacher))+ggtitle(paste(title_str,"充足数VS足切り率"))
gg5=g2 + geom_point()+stat_smooth()+xlab("足切り率")+ylab("充足数")
g3=ggplot(data.frame(cbind(eval_teacher,filled_teacher)),aes(eval_teacher,filled_teacher))+ggtitle(paste(title_str,"評価回数VS足切り率"))
gg6=g3 + geom_point()+stat_smooth()+xlab("評価回数")+ylab("充足数")
